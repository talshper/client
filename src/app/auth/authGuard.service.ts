import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UsersService} from '../services/users.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public usersService: UsersService, public router: Router) {
  }

  canActivate(): boolean {
    if (!this.usersService.isUserLoggedIn()) {
      this.router.navigate(['signIn']);
      return false;
    }
    return true;
  }
}
