import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MovieDetailsComponent } from './movies/movie-details/movie-details.component';
import { MovieDetailsResolver } from './movies/movie-details.resolver';

import { SignInComponent } from './sign-in/sign-in.component';
import { AuthGuardService } from './auth/authGuard.service';
import { UserReviewsComponent } from './user/user-reviews/user-reviews.component';
import { UserReviewsResolver } from './user/user-reviews.resolver';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { UserDetailsResolver } from './users/user-details/user-details.resolver';
import { StatisticsComponent } from './statistics/statistics/statistics.component';
import { StatisticsResolver } from './statistics/statistics.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'movie-details/:id',
    component: MovieDetailsComponent,
    canActivate: [AuthGuardService],
    resolve: {
      movieDetails: MovieDetailsResolver
    }
  },
  {
    path: 'user-details',
    component: UserDetailsComponent,
    canActivate: [AuthGuardService],
    resolve: {
      userDetails: UserDetailsResolver
    }
  },
  {
    path: 'profile/my-reviews',
    component: UserReviewsComponent,
    canActivate: [AuthGuardService],
    resolve: {
      userReviews: UserReviewsResolver
    }
  },
  {
    path: 'signIn',
    component: SignInComponent
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
    canActivate: [AuthGuardService],
    resolve: {
      statistics: StatisticsResolver
    }
  },
  {
    path: '**',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
