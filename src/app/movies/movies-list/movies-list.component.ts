import {Component, OnInit} from '@angular/core';
import {Movie} from '../model/movie.model';
import {MovieService} from '../../services/movie.service';
import {GenreService} from '../../services/genre.service';
import {Genre} from '../model/genre.model';

@Component({
  selector: ' app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  movies: Movie[] = [];
  genres: Genre[] = [];
  searchText = '';
  isRatingFilterChecked: boolean;
  selectedGenreId = '';
  _ratingFilterValue = 1;

  get ratingFilterValue(): number {
    return this.isRatingFilterChecked ? this._ratingFilterValue : 0;
  }

  constructor(private movieService: MovieService,
              private genreService: GenreService) {
  }

  ngOnInit(): void {
    this.movieService.getAllMovies().subscribe(movies => {
      this.movies = movies;
    });

    this.genreService.getGenres().subscribe(genres => {
      this.genres = genres;
    });

    this.movieService.getMovieSubject().subscribe((newMovie: Movie) => {
      if (!!newMovie) {
        this.movies.map((movie) => movie.title).includes(newMovie.title) ?
          alert('movie already exist!') : this.createNewMovie(newMovie);
      }
    });
  }

  private createNewMovie(newMovie: Movie): void {
    this.movieService.createMovie(newMovie).subscribe((movie: Movie) => {
      this.movies.unshift(movie);
    });
  }

  onRatingChange(ratingValue: number) {
    this._ratingFilterValue = ratingValue;
  }
}
