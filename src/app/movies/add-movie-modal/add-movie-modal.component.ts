import {Component, OnInit} from '@angular/core';
import {MDBModalRef} from 'angular-bootstrap-md';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MovieService} from '../../services/movie.service';
import {Movie} from '../model/movie.model';

@Component({
  selector: 'app-verification-modal',
  templateUrl: './add-movie-modal.component.html',
  styleUrls: ['./add-movie-modal.component.scss']
})
export class AddMovieModalComponent implements OnInit {
  movieForm: FormGroup;
  movieName: string;
  moviesResult: any = [];
  selectedMovie: any;
  imageCdn = 'http://image.tmdb.org/t/p/w500/';

  constructor(public modalRef: MDBModalRef,
              private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.movieForm = new FormGroup({
      movieNameForm: new FormControl('', [Validators.required])
    });
  }

  get movieNameForm() {
    return this.movieForm.get('movieNameForm');
  }

  searchMovie(): void {
    this.movieService.getMoviesByName(this.movieName).subscribe(value => {
      this.moviesResult = value.results;
    });
  }

  onSubmit() {
    const newMovie: Movie = {
      _id: '', externalApiId: this.selectedMovie.id,
      title: this.selectedMovie.title, description: this.selectedMovie.overview, genresApiIds: this.selectedMovie.genre_ids,
      imageUrl: this.imageCdn + this.selectedMovie.poster_path, rating: 0
    };

    this.movieService.sendNewMovie(newMovie);

    this.modalRef.hide();
  }

  selectMovie(movie: any): void {
    this.selectedMovie = movie;
  }
}
