import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  BadgeModule,
  ButtonsModule,
  CardsModule,
  CheckboxModule,
  IconsModule,
  InputsModule,
  InputUtilitiesModule,
  WavesModule
} from 'angular-bootstrap-md';
import {MovieCardComponent} from './movie-card/movie-card.component';
import {HttpClientModule} from '@angular/common/http';
import {MovieDescriptionPipe} from '../pipes/movie-description.pipe';
import {MoviesListComponent} from './movies-list/movies-list.component';
import {FilterMoviePipe} from '../pipes/filter-movie.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MovieDetailsComponent} from './movie-details/movie-details.component';
import {RouterModule} from '@angular/router';
import { ReviewsModule } from '../reviews/reviews.module';
import {MovieRatingFilterPipe} from '../pipes/movie-rating-filter.pipe';
import {GenreFilterPipe} from '../pipes/genre-filter.pipe';
import {AddMovieModalComponent} from './add-movie-modal/add-movie-modal.component';

@NgModule({
  declarations: [
    MovieCardComponent,
    MoviesListComponent,
    MovieDescriptionPipe,
    FilterMoviePipe,
    MovieRatingFilterPipe,
    GenreFilterPipe,
    MovieDetailsComponent,
    AddMovieModalComponent
  ],
  exports: [
    MovieCardComponent,
    MoviesListComponent
  ],
  imports: [
    CommonModule,
    CardsModule,
    WavesModule,
    CheckboxModule,
    ButtonsModule,
    HttpClientModule,
    FormsModule,
    InputsModule,
    RouterModule,
    IconsModule,
    BadgeModule,
    ReactiveFormsModule,
    InputUtilitiesModule,
    ReviewsModule
  ]
})
export class MoviesModule {
}
