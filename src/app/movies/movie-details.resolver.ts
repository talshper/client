import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {MovieService} from '../services/movie.service';
import {MovieDetails} from './model/movie-details.model';
import {defaultIfEmpty, map, mergeMap} from 'rxjs/operators';
import {GenreService} from '../services/genre.service';
import {Movie} from './model/movie.model';
import {ReviewService} from '../services/review.service';
import {ReviewPayload} from './model/reviewPayload.model';
import {UsersService} from '../services/users.service';
import {User} from '../models/user.model';
import {Review} from './model/review.model';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsResolver implements Resolve<MovieDetails> {
  constructor(private movieService: MovieService,
              private genreService: GenreService,
              private reviewService: ReviewService,
              private usersService: UsersService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MovieDetails> {
    return this.movieService.getMovieById(route.paramMap.get('id')).pipe(
      mergeMap((movie: Movie) => forkJoin({
        movie: of(movie),
        genres: this.genreService.getGenresByIds(movie.genresApiIds),
        reviewsPayload: this.getReviewsPayload(movie._id)
      }))
    );
  }

  getReviewsPayload(movieId: string): Observable<ReviewPayload[]> {
    return this.reviewService.getReviewsByMovieId(movieId).pipe(
      mergeMap(reviews => forkJoin(
        reviews.map(
          (review: Review) => this.usersService.getUserById(review.reviewerId).pipe(map((user: User) => {
            return new ReviewPayload(review._id, review.content, user.username, review.rating);
          }))
        )).pipe(defaultIfEmpty([]))
      ));
  }
}
