import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieDetails } from '../model/movie-details.model';
import { ReviewService } from '../../services/review.service';
import { UsersService } from '../../services/users.service';
import { Review, ReviewContent } from '../model/review.model';
import { ReviewPayload } from '../model/reviewPayload.model';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  movieDetails: MovieDetails;

  constructor(private activatedRoute: ActivatedRoute,
              private reviewService: ReviewService,
              private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((result) => {
      this.movieDetails = result.movieDetails;
    });
  }

  userHasComment(): boolean {
    return !!this.movieDetails.reviewsPayload.find(review => review.username === this.usersService.getUser().username);
  }

  onSubmitReview(reviewContent: ReviewContent): void {
    const review: Review = {
      _id: '',
      content: reviewContent.content,
      movieId: this.movieDetails.movie._id,
      rating: reviewContent.rating,
      reviewerId: this.usersService.getUser()._id
    };

    this.reviewService.addReview(review).subscribe((newReview: Review) => {
      this.movieDetails.reviewsPayload.unshift(new ReviewPayload(newReview._id, newReview.content, this.usersService.getUser().username, newReview.rating));
      this.movieDetails.reviewsPayload = [...this.movieDetails.reviewsPayload];
    });
  }

  onEditReview({id, content, rating}: ReviewPayload) {
    this.reviewService.updateReview({_id: id, content, rating}).subscribe((newReview: Review) => {
      this.movieDetails.reviewsPayload[0] = (new ReviewPayload(newReview._id, newReview.content, this.usersService.getUser().username, newReview.rating));
      this.movieDetails.reviewsPayload = [...this.movieDetails.reviewsPayload];
    });
  }

  onDeleteReview(id: string) {
    this.reviewService.deleteReview(id).subscribe(message => {
      const index = this.movieDetails.reviewsPayload.findIndex((review) => review.id === id);
      this.movieDetails.reviewsPayload.splice(index, 1);
      this.movieDetails.reviewsPayload = [...this.movieDetails.reviewsPayload];
    });
  }
}
