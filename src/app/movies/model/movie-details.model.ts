import {Movie} from './movie.model';
import {Genre} from './genre.model';
import {ReviewPayload} from './reviewPayload.model';

export interface MovieDetails {
  movie: Movie;
  genres: Genre[];
  reviewsPayload: ReviewPayload[];
}
