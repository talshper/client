export class ReviewPayload {
  constructor(public id: string,
              public content: string,
              public username: string,
              public rating: number) {
  }
}
