export interface Movie {
  _id: string;
  externalApiId: string;
  title: string;
  description: string;
  rating: number;
  imageUrl: string;
  genresApiIds: string[];
}
