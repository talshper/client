export interface ReviewContent {
  content: string;
  rating: number;
}

export interface Review extends ReviewContent {
  _id: string;
  movieId: string;
  reviewerId: string;
}
