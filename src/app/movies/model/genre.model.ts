export interface Genre {
  externalAPIid: string;
  name: string;
}
