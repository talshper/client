import {Component, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (!this.usersService.isUserLoggedIn()) {
      this.router.navigateByUrl('signIn');
    }
  }

}
