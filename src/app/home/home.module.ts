import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MoviesModule} from '../movies/movies.module';
import {HomeComponent} from './home.component';
import {UsersModule} from "../users/users.module";

@NgModule({
  declarations: [HomeComponent],
  exports: [HomeComponent],
  imports: [
    CommonModule,
    MoviesModule,
    UsersModule
  ]
})
export class HomeModule {
}
