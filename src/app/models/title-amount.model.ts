export interface TitleAmount {
  title: string;
  amount: number;
}
