export interface Reviewable {
  rating: number;
}
