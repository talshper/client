import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: "[appNameHover]"
})
export class AppNameHoverDirective {
  elementRef: ElementRef;

  constructor(private el: ElementRef,
              private renderer: Renderer2) {
    this.elementRef = el;
  }

  @HostListener('mouseover')
  onMouseOver() {
    this.renderer.addClass(this.elementRef.nativeElement, 'shake-brand');
  }

  @HostListener('mouseout')
  onMouseOut() {
    this.renderer.removeClass(this.elementRef.nativeElement, 'shake-brand');
  }
}
