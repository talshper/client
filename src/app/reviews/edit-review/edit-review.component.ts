import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ReviewContent } from '../../movies/model/review.model';
import { MovieRatingComponent } from '../movie-rating/movie-rating.component';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.component.html',
  styleUrls: ['./edit-review.component.scss']
})
export class EditReviewComponent implements OnInit {
  @Input() content: string;
  @Input() rating: number = 1;
  @Output() submitReview = new EventEmitter<ReviewContent>();
  @ViewChild(MovieRatingComponent) ratingComponent!: MovieRatingComponent;
  commentFormGroup: FormGroup;

  ngOnInit(): void {
    this.commentFormGroup = new FormGroup({
      comment: new FormControl(this.content, [Validators.required, this.noWhitespaceValidator])
    });
  }

  onSubmit(): void {
    this.submitReview.emit({rating: this.rating, content: this.getComment.value.trim()})
  }

  get getComment() {
    return this.commentFormGroup.get('comment');
  }

  private noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : {whitespace: true};
  }
}
