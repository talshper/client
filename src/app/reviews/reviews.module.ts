import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { ReviewComponent } from './review-list/review/review.component';
import {
  BadgeModule, ButtonsModule, CardsModule,
  CheckboxModule,
  IconsModule,
  InputsModule,
  InputUtilitiesModule,
  WavesModule
} from 'angular-bootstrap-md';
import { ReviewListComponent } from './review-list/review-list.component';
import { EditReviewComponent } from './edit-review/edit-review.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieRatingComponent } from './movie-rating/movie-rating.component';
import { ReviewContentFilterPipe } from './pipes/review-content-filter.pipe';
import { ReviewerNameFilterPipe } from './pipes/reviewer-name-filter.pipe';
import { ReviewRatingFilterPipe } from './pipes/review-rating-filter.pipe';

@NgModule({
  declarations: [
    EditReviewComponent,
    ReviewComponent,
    ReviewListComponent,
    MovieRatingComponent,
    ReviewContentFilterPipe,
    ReviewRatingFilterPipe,
    ReviewerNameFilterPipe
  ],
  exports: [
    EditReviewComponent,
    ReviewListComponent,
    MovieRatingComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    InputsModule,
    WavesModule,
    ReactiveFormsModule,
    CheckboxModule,
    FormsModule,
    BadgeModule,
    ButtonsModule,
    CardsModule,
    InputUtilitiesModule
  ]
})
export class ReviewsModule {
}
