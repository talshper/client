import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-movie-rating',
  templateUrl: './movie-rating.component.html',
  styleUrls: ['./movie-rating.component.scss']
})
export class MovieRatingComponent {
  @Input() rating: number = 1;
  @Output() ratingChange = new EventEmitter<number>();
  @Input() isDisabled: boolean = false;

  rate(ratingNumber: number): void {
    this.rating = ratingNumber;
    this.ratingChange.emit(this.rating);
  }
}
