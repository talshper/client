import {Pipe, PipeTransform} from '@angular/core';
import {ReviewPayload} from "../../movies/model/reviewPayload.model";

@Pipe({
  name: 'reviewContentFilter'
})
export class ReviewContentFilterPipe implements PipeTransform {
  transform(items: ReviewPayload[], searchText: string): ReviewPayload[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.content.toLocaleLowerCase().includes(searchText);
    });
  }
}
