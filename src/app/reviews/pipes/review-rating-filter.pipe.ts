import {Pipe, PipeTransform} from '@angular/core';
import {ReviewPayload} from "../../movies/model/reviewPayload.model";

@Pipe({
  name: 'reviewRatingFilter'
})
export class ReviewRatingFilterPipe implements PipeTransform {
  transform(items: ReviewPayload[], ratingValue: number): ReviewPayload[] {
    if (!items) {
      return [];
    } else if (!ratingValue) {
      return items;
    }

    return items.filter(it => it.rating === ratingValue);
  }
}
