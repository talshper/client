import {Pipe, PipeTransform} from '@angular/core';
import {ReviewPayload} from "../../movies/model/reviewPayload.model";

@Pipe({
  name: 'reviewerNameFilter'
})
export class ReviewerNameFilterPipe implements PipeTransform {
  transform(items: ReviewPayload[], reviewerName: string): ReviewPayload[] {
    if (!items) {
      return [];
    }
    if (!reviewerName) {
      return items;
    }
    reviewerName = reviewerName.toLocaleLowerCase();

    return items.filter(it => {
      return it.username.toLocaleLowerCase().includes(reviewerName);
    });
  }
}
