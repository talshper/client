import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ReviewPayload } from '../../../movies/model/reviewPayload.model';
import { ReviewContent } from '../../../movies/model/review.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent {
  @Input() review: ReviewPayload;
  @Input() isEditable: boolean;
  @Output() onEditReview = new EventEmitter<ReviewPayload>();
  @Output() onDeleteReview = new EventEmitter<string>();
  isDuringEdit: boolean = false;

  editReview(editedReview: ReviewContent) {
    this.onEditReview.emit({...this.review, ...editedReview});
    this.isDuringEdit = false;
  }

  deleteReview(id: string) {
    this.onDeleteReview.emit(id);
  }
}
