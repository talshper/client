import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ReviewPayload } from '../../movies/model/reviewPayload.model';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})
export class ReviewListComponent implements OnInit {
  @Input() reviews: ReviewPayload[];
  @Output() onEditReview = new EventEmitter<ReviewPayload>();
  @Output() onDeleteReview = new EventEmitter<string>();
  isRatingFilterChecked: boolean;
  reviewContentToSearch: string;
  userReviewerToSearch: string;
  _ratingFilterValue = 1;

  get ratingFilterValue(): number {
    return this.isRatingFilterChecked ? this._ratingFilterValue : 0;
  }

  constructor(public usersService: UsersService) {
  }

  ngOnInit() {
    this.reviews = this.reviews.sort(
      (x, y) =>
        x.username == this.usersService.getUser().username ?
        -1 :
          y.username == this.usersService.getUser().username ? 1 : 0)
  }

  editReview(editedReview: ReviewPayload) {
    this.onEditReview.emit(editedReview);
  }

  deleteReview(id: string) {
    this.onDeleteReview.emit(id);
  }

  onRatingChange(ratingValue: number) {
    this._ratingFilterValue = ratingValue;
  }
}
