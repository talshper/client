import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule, NavbarModule } from 'angular-bootstrap-md';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeModule } from './home/home.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignInModule } from './sign-in/sign-in.module';
import { AuthGuardService } from './auth/authGuard.service';
import { UserModule } from './user/user.module';
import { AppNameHoverDirective } from './directives/app-name-hover.directive';
import { WsService } from './services/ws-service.service';
import { StatisticsModule } from './statistics/statistics.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './error-interceptor/http-error.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AppNameHoverDirective
  ],
  imports: [
    StatisticsModule,
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    NavbarModule,
    SignInModule,
    HomeModule,
    UserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true
  }, AuthGuardService, WsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
