import {Component, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  validatingForm: FormGroup;

  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.validatingForm = new FormGroup({
      username: new FormControl(null,
        [Validators.minLength(4), Validators.required]),
      password: new FormControl(null,
        [Validators.minLength(4), Validators.required]),
    });
  }

  login() {
    const formFields = this.validatingForm.controls;

    this.usersService.login(formFields.username.value, formFields.password.value).pipe(first())
      .subscribe(() => {
        this.router.navigateByUrl('');
      });
  }
}
