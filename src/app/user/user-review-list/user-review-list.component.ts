import { Component, Input } from '@angular/core';
import { ReviewWithMoviePayload } from '../model/review-with-movie-payload.model';

@Component({
  selector: 'app-user-review-list',
  templateUrl: './user-review-list.component.html',
  styleUrls: ['./user-review-list.component.scss']
})
export class UserReviewListComponent {
  @Input() reviews: ReviewWithMoviePayload[];
}
