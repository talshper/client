import { Component, Input } from '@angular/core';
import { ReviewWithMoviePayload } from '../../model/review-with-movie-payload.model';

@Component({
  selector: 'app-user-review-row',
  templateUrl: './user-review-row.component.html',
  styleUrls: ['./user-review-row.component.scss']
})
export class UserReviewRowComponent {
  @Input() review: ReviewWithMoviePayload;
}
