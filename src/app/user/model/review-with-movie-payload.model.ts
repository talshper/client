export interface ReviewWithMoviePayload {
  content: string;
  rating: number;
  movie: {
    _id: string;
    imageUrl: string;
    title: string;
  }
}
