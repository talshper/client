import { User } from '../../models/user.model';
import { ReviewWithMoviePayload } from './review-with-movie-payload.model';

export interface UserReviews {
  user: User;
  reviews: ReviewWithMoviePayload[];
}
