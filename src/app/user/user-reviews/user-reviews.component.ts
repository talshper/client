import { Component, OnInit } from '@angular/core';
import { UserReviews } from '../model/user-reviews.model';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-reviews',
  templateUrl: './user-reviews.component.html',
  styleUrls: ['./user-reviews.component.scss']
})
export class UserReviewsComponent implements OnInit {
  userReviews$: Observable<UserReviews>;
  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.userReviews$ = this.activatedRoute.data.pipe(map(data => data.userReviews));
  }
}
