import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable} from 'rxjs';
import { defaultIfEmpty, map } from 'rxjs/operators';
import { ReviewService } from '../services/review.service';
import { UsersService } from '../services/users.service';
import { UserReviews } from './model/user-reviews.model';
import { ReviewWithMoviePayload } from './model/review-with-movie-payload.model';

@Injectable({
  providedIn: 'root'
})
export class UserReviewsResolver implements Resolve<UserReviews> {
  constructor(private reviewService: ReviewService,
              private usersService: UsersService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserReviews> {
    return this.getReviewsPayload(this.usersService.getUser()._id).pipe(map(reviewsPayload => ({
      user: this.usersService.getUser(),
      reviews: reviewsPayload
    })));
  }

  getReviewsPayload(userId: string): Observable<ReviewWithMoviePayload[]> {
    return this.reviewService.getReviewsByUserId(userId).pipe(defaultIfEmpty([]));
  }
}
