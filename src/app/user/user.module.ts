import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserReviewsComponent } from './user-reviews/user-reviews.component';
import { UserReviewListComponent } from './user-review-list/user-review-list.component';
import { UserReviewRowComponent } from './user-review-list/user-review-row/user-review-row.component';
import { IconsModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    UserReviewsComponent,
    UserReviewListComponent,
    UserReviewRowComponent
  ],
  exports: [],
  imports: [
    CommonModule,
    IconsModule,
    RouterModule
  ]
})
export class UserModule {
}
