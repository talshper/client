import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {UsersService} from '../services/users.service';
import {User} from '../models/user.model';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  validatingForm: FormGroup;
  user: User;
  @Input() isUpdateMode: boolean;

  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.validatingForm = new FormGroup(this.getFormControls());
    this.setUserDetailsFields();
  }

  private getFormControls() {
    let formControls = {
      username: new FormControl(null,
        [Validators.minLength(4), Validators.required]),
      firstName: new FormControl(null,
        [this.forbiddenNameValidator()]),
      lastName: new FormControl(null,
        [this.forbiddenNameValidator()])
    };

    if (!this.isUpdateMode) {
      formControls['password'] = new FormControl(null,
        [Validators.minLength(4), Validators.required]);
      formControls['repeatedPassword'] = new FormControl(null,
        [this.passwordRepeatValidation()]);
    }

    return formControls;
  }

  private setUserDetailsFields() {
    if (this.isUpdateMode) {
      this.user = this.usersService.getUser();

      this.validatingForm.setValue({
        username: this.user.username,
        firstName: this.user.firstName,
        lastName: this.user.lastName
      });
    }
  }

  forbiddenNameValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.value && control.value.trim().length ? null : {forbiddenName: {value: control.value}};
    };
  }

  passwordRepeatValidation(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.value && control.value === this.validatingForm.controls.password.value
      && this.validatingForm.controls.password.valid
        ? null : {forbiddenName: {value: control.value}};
    };
  }

  register() {
    const formFields = this.validatingForm.controls;
    const newUser: User = {
      _id: '',
      username: formFields.username.value,
      firstName: formFields.firstName.value,
      lastName: formFields.lastName.value,
      password: formFields.password.value
    };

    this.usersService.register(newUser).pipe(first()).subscribe(() => {
      this.router.navigateByUrl('');
    }, error => {
      alert(error.error);
    });
  }

  updateUserDetails() {
    const newUser = new User(this.user._id, this.validatingForm.controls.username.value, this.user.password,
      this.validatingForm.controls.firstName.value, this.validatingForm.controls.lastName.value);
    this.usersService.updateUserDetails(newUser).subscribe(() => {
      this.router.navigateByUrl('');
    });
  }
}
