import {Component, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  isLogin = true;

  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (this.usersService.isUserLoggedIn()) {
      this.router.navigateByUrl('');
    }
  }

  setIsLogin(isLogin: boolean) {
    this.isLogin = isLogin;
  }
}
