import {Pipe, PipeTransform} from '@angular/core';
import {Movie} from '../movies/model/movie.model';

@Pipe({
  name: 'movieRatingFilter'
})
export class MovieRatingFilterPipe implements PipeTransform {
  transform(items: Movie[], ratingValue: number): Movie[] {
    if (!items) {
      return [];
    } else if (!ratingValue) {
      return items;
    }

    return items.filter(it => it.rating >= ratingValue);
  }
}
