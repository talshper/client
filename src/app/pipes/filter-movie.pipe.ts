import {Pipe, PipeTransform} from '@angular/core';
import {Movie} from '../movies/model/movie.model';

@Pipe({
  name: 'filterMovie'
})
export class FilterMoviePipe implements PipeTransform {
  transform(items: Movie[], searchText: string): Movie[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.title.toLocaleLowerCase().includes(searchText);
    });
  }
}
