import {Pipe, PipeTransform} from '@angular/core';
import {Movie} from '../movies/model/movie.model';

@Pipe({
  name: 'genreFilter'
})
export class GenreFilterPipe implements PipeTransform {
  transform(items: Movie[], genreId: string): Movie[] {
    if (!items) {
      return [];
    } else if (!genreId) {
      return items;
    }

    return items.filter(it => it.genresApiIds.includes(genreId));
  }
}
