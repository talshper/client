import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { WsService } from './services/ws-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'client';
  isSignInPage = false;

  constructor(private router: Router, private wsService: WsService) {
  }

  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isSignInPage = (val.url === '/signIn');
      }
    });
  }
}
