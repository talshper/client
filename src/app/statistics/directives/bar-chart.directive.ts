import { Directive, ElementRef, HostListener, Input, OnChanges } from '@angular/core';
import * as d3 from 'd3';
import { TitleAmount } from '../../models/title-amount.model';


@Directive({
  selector: "[appBarChart]"
})
export class BarChartDirective implements OnChanges {
  @Input() chartData: TitleAmount[];
  elementRef: ElementRef;
  margin = {top: 20, right: 40, bottom: 30, left: 40};

  constructor(private el: ElementRef) {
    this.elementRef = el;
  }

  ngOnChanges(): void {
    if (!this.chartData) { return; }

    this.createChart();
  }

  @HostListener('window:resize') onResize() {
    this.createChart();
  }

  private createChart(): void {
    d3.select('svg').remove();

    const element = this.elementRef.nativeElement;
    const data = this.chartData;

    const svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.1)
      .domain(data.map(d => d.title));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, d3.max(data, d => d.amount)]);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x));

    g.append('g')
      .attr('class', 'axis axis--y')
      .call(d3.axisLeft(y).ticks(10))
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('text-anchor', 'end')
      .text('Amount');

    g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
      .attr('class', 'bar')
      .attr('style', 'fill: steelblue')
      .attr('x', d => x(d.title))
      .attr('y', d => y(d.amount))
      .attr('width', x.bandwidth())
      .attr('height', d => contentHeight - y(d.amount));
  }
}
