import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable} from 'rxjs';
import { TitleAmount } from '../models/title-amount.model';
import { MovieService } from '../services/movie.service';

@Injectable({
  providedIn: 'root'
})
export class StatisticsResolver implements Resolve<TitleAmount[]> {
  constructor(private movieService: MovieService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TitleAmount[]> {
    return this.movieService.getGenreStatistics();
  }
}
