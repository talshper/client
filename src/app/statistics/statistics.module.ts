import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsComponent } from './statistics/statistics.component';
import { BarChartDirective } from './directives/bar-chart.directive';

@NgModule({
  declarations: [
    StatisticsComponent,
    BarChartDirective
  ],
  exports: [
    StatisticsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class StatisticsModule {
}
