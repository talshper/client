import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserDetailsComponent} from "./user-details/user-details.component";
import {SignInModule} from "../sign-in/sign-in.module";



@NgModule({
  declarations: [UserDetailsComponent],
  imports: [
    CommonModule,
    SignInModule
  ]
})
export class UsersModule { }
