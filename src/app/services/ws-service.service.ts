import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
import { io } from 'socket.io-client';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WsService {
  socket = io(environment.serverUrl, { autoConnect: false, transports: ['websocket'] })
  constructor(private usersService: UsersService) {
    usersService.getLoggedInUserSubject().subscribe(user => {
      if (user) {
        this.socket.open();
      } else {
        this.socket.close();
      }
    })
  }
}
