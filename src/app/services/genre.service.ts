import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Genre} from '../movies/model/genre.model';

@Injectable({
  providedIn: 'root'
})
export class GenreService {
  private genreUrl: string = environment.serverUrl + 'genres';

  constructor(private http: HttpClient) {
  }

  getGenresByIds(ids: string[]): Observable<Genre[]> {
    return this.http.get<Genre[]>(this.genreUrl + '/' + ids);
  }

  getGenres(): Observable<Genre[]> {
    return this.http.get<Genre[]>(this.genreUrl);
  }
}
