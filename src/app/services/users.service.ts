import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { BehaviorSubject, Observable, of } from 'rxjs';
import {mergeMap} from "rxjs/operators";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private usersUrl: string = environment.serverUrl + 'users';
  private loginUrl: string = this.usersUrl + '/signIn';
  private userKey = 'USER';
  private loggedInUserSubject = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, private router: Router) {
    if (this.isUserLoggedIn()) {
      this.loggedInUserSubject.next(this.getUser());
    }
  }

  getLoggedInUserSubject(): Observable<User> {
    return this.loggedInUserSubject.asObservable();
  }

  getUserById(id: string): Observable<User> {
    return this.http.get<User>(this.usersUrl + '/' + id);
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post<User>(this.loginUrl, {
      username, password
    }).pipe(mergeMap((user: User) => this.saveUserObservable(user)));
  }

  register(user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl, user)
      .pipe(mergeMap((user: User) => this.saveUserObservable(user)));
  }

  saveUser(user: User): void {
    localStorage.setItem(this.userKey, JSON.stringify(user));
  }

  getUser(): User {
    return JSON.parse(localStorage.getItem(this.userKey));
  }

  updateUserDetails(user: User) {
    return this.http.put<User>(`${this.usersUrl}/${user._id}`, user)
      .pipe(mergeMap((user: User) => this.saveUserObservable(user)));
  }

  private saveUserObservable(user: User) {
    this.saveUser(user);
    this.loggedInUserSubject.next(user);
    return of(user);
  }

  isUserLoggedIn(): boolean {
    return !!localStorage.getItem(this.userKey);
  }

  logout(): void {
    localStorage.removeItem(this.userKey);
    this.loggedInUserSubject.next(null);
    this.router.navigateByUrl('signIn');
  }
}
