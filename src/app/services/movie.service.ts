import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {Movie} from '../movies/model/movie.model';
import {TitleAmount} from '../models/title-amount.model';
import {User} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private moviesUrl: string = environment.serverUrl + 'movies';
  private moviesApiUrl = 'https://api.themoviedb.org/3';
  private newMovieSubject = new BehaviorSubject<Movie>(null);

  constructor(private http: HttpClient) {
  }

  getAllMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.moviesUrl);
  }

  getMovieById(id: string): Observable<Movie> {
    return this.http.get<Movie>(this.moviesUrl + '/' + id);
  }

  createMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(this.moviesUrl, movie);
  }

  getGenreStatistics(): Observable<TitleAmount[]> {
    return this.http.get<TitleAmount[]>(this.moviesUrl + '/genreStatistics');
  }

  getMoviesByName(movieName: string): Observable<any> {
    return this.http.get<any>(this.moviesApiUrl + '/search/movie?api_key=feff4325c52243b8d8d8220ab5105f5b&query=' + movieName
    );
  }

  sendNewMovie(movie: Movie): void {
    this.newMovieSubject.next(movie);
  }

  getMovieSubject(): Observable<Movie> {
    return this.newMovieSubject.asObservable();
  }
}
