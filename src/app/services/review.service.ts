import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Review} from '../movies/model/review.model';
import { ReviewWithMoviePayload } from '../user/model/review-with-movie-payload.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  private reviewUrl: string = environment.serverUrl + 'reviews';

  constructor(private http: HttpClient) {
  }

  getReviewsByMovieId(id: string): Observable<Review[]> {
    return this.http.get<Review[]>(this.reviewUrl + '/movie-reviews/' + id);
  }

  getReviewsByUserId(id: string): Observable<ReviewWithMoviePayload[]> {
    return this.http.get<ReviewWithMoviePayload[]>(this.reviewUrl + '/user/' + id);
  }

  addReview(review: Review): Observable<Review> {
    return this.http.post<Review>(this.reviewUrl, review);
  }

  updateReview(review: Partial<Review>): Observable<Review> {
    return this.http.put<Review>(`${this.reviewUrl}/${review._id}`, { review });
  }

  deleteReview(id: string): Observable<any> {
    return this.http.delete<any>(`${this.reviewUrl}/${id}`);
  }
}
