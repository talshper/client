import {Component, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {Router} from '@angular/router';
import {User} from '../models/user.model';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import {MDBModalRef, MDBModalService} from 'angular-bootstrap-md';
import {AddMovieModalComponent} from '../movies/add-movie-modal/add-movie-modal.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  userFullName: Observable<string>;
  modalRef: MDBModalRef;

  constructor(private usersService: UsersService,
              private router: Router,
              private modalService: MDBModalService) {
  }

  ngOnInit(): void {
    this.userFullName = this.usersService.getLoggedInUserSubject().pipe(
      filter(user => user !== null),
      map(this.getFullName)
    );
  }

  private getFullName(user: User): string {
    return user.firstName.concat(' ', user.lastName);
  }

  logout(): void {
    this.usersService.logout();
  }

  navigateToUserDetailsScreen() {
    this.router.navigateByUrl('user-details');
  }

  openAddMovieModal() {
    this.modalRef = this.modalService.show(AddMovieModalComponent);
  }
}
